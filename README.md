The problem statement can be found in the doucument BankAccountTransactionProblem.txt.

This porject is developed using Java 8, Spring boot, JPA, inbuilt H2 database, Maven.

How to run project:-

1. Download the project.
2. Data will be loaded from the data.sql file to inbuilt H2database. You can also enter the data using REST api's using create transaction API.
2. Open code base in IntelIj.
3. Maven clean install.
4. Run BankTransactionApplication class.
5. In order to open inbuilt H2 database using the link - http://localhost:8085/h2-console/      URL:jdbc:h2:mem:testdb

______________________________________________________________________________________________

REST end points:-

1. To check the relative account balace:

GET - http://localhost:8085/balance 

BODY:
{
  "accountId": "ACC334455",
  "fromDate": "2018-10-20T12:00:00.000Z",
  "toDate": "2018-10-20T19:00:00.000Z"
}

Sample Response:
{
    "relativeBalance": -25.00,
    "noOfTransactions": 1
}
______________________________________________________________________________________________

2. Create Transaction:

POST : http://localhost:8085/transaction

BODY: 
{
  "fromAccountId": "ACC334455",
  "toAccountId": "ACC778899",
  "createAt": "2012-01-30",
  "amount": 25.00,
  "transactionType": "PAYMENT"
}
______________________________________________________________________________________________

3. Get Transaction by ID - GET - http://localhost:8085/transaction/1

______________________________________________________________________________________________

4. Get all Transactions - GET - http://localhost:8085/transactions

______________________________________________________________________________________________

5. Update a Transaction - PUT - http://localhost:8085/transaction/1

BODY: 
{
  "fromAccountId": "ACC334455",
  "toAccountId": "ACC778899",
  "createAt": "2012-01-30",
  "amount": 25.00,
  "transactionType": "PAYMENT"
}
______________________________________________________________________________________________

6. Delete a Transaction - DELETE - http://localhost:8085/transaction/1

______________________________________________________________________________________________



