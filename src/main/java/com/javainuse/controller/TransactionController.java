package com.javainuse.controller;

import com.javainuse.entity.SearchCriteria;
import com.javainuse.entity.SearchResponse;
import com.javainuse.exception.TransactionCustomException;
import com.javainuse.entity.Transaction;
import com.javainuse.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class TransactionController {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);

	@Autowired
	private TransactionService transactionService;

	@ResponseBody
	@RequestMapping(value = "/balance", method = RequestMethod.GET)
	public SearchResponse fetchTransaction(@RequestBody SearchCriteria criteria) {
		if(transactionService.validateSearchRequest(criteria)) {
			LOG.debug("Getting relative balance for account id:" + criteria.getAccountId());
			return transactionService.getRelativeBalance(criteria);
		}
		return null;
	}

    @ResponseBody
	@RequestMapping(value = "/transaction", method = RequestMethod.POST)
	public Long newTransaction(@RequestBody Transaction transaction, HttpServletResponse response) {
		LOG.debug("Creating new Transaction.");
		Long transactionId = null;
		boolean validate = transactionService.validateRequest(transaction);

		if(validate)
		{
			transactionId = transactionService.saveTransaction(transaction);
		}
		else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return transactionId;
	}

	@ResponseBody
	@RequestMapping(value = "/transaction/{id}", method = RequestMethod.GET)
	public Transaction getTransactionById(@PathVariable Long id) {
		LOG.debug("Get transaction for the id:" + id);
		Transaction transaction = transactionService.findTransactionById(id);
		return transaction;
	}

	@ResponseBody
	@RequestMapping(value = "/transactions", method = RequestMethod.GET)
	public List<Transaction> getTransactionsList() {
		LOG.debug("Get all the transactions");
		List<Transaction> allTransactions = transactionService.getTransactionsList();
		return allTransactions;
	}

	@ResponseBody
	@RequestMapping(value = "/transaction/{id}", method = RequestMethod.PUT)
	public Transaction updateTransaction(@PathVariable Long id ,@RequestBody Transaction transaction
			, HttpServletResponse response) throws TransactionCustomException {
		LOG.debug("Update transaction for the id" + id);
		Transaction updatedTransaction = null;
		boolean validate = transactionService.validateRequest(transaction);

		if(validate)
		{
			updatedTransaction = transactionService.updateTransaction(id, transaction);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return updatedTransaction;
	}

	@RequestMapping(value = "/transaction/{id}", method = RequestMethod.DELETE)
	public ResponseEntity.BodyBuilder deleteTransactionById(@PathVariable Long id) throws TransactionCustomException {
		LOG.debug("Delete transaction for the id" + id);
		transactionService.deleteTransactionById(id);
		return ResponseEntity.status(HttpStatus.OK);
	}

}
