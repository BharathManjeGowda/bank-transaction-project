package com.javainuse.dao;

import com.javainuse.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Transaction findByTransactionId(Long id);

    @Query("FROM Transaction t WHERE t.fromAccountId =  :accountId AND t.createAt > :fromDate AND t.createAt < :toDate")
    List<Transaction> findAllOutgoingTransactions(@Param("accountId") String accountId, @Param("fromDate") Calendar fromDate, @Param("toDate") Calendar toDate);

    @Query("FROM Transaction t WHERE t.toAccountId =  :accountId AND t.createAt > :fromDate AND t.createAt < :toDate")
    List<Transaction> findAllIncomingTransactions(@Param("accountId") String accountId, @Param("fromDate") Calendar fromDate, @Param("toDate") Calendar toDate);

    @Query("select t.relatedTransaction FROM Transaction t WHERE t.transactionType= 'REVERSAL' AND (t.fromAccountId =  :accountId OR t.toAccountId =  :accountId)")
    List<Long> findAllByTransactionTypeAndRelatedTransaction(@Param("accountId") String accountId);

}
