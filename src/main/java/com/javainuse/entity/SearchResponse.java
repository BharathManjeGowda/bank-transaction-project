package com.javainuse.entity;

import java.math.BigDecimal;

public class SearchResponse {

	private BigDecimal relativeBalance;
	private	int noOfTransactions;

	public BigDecimal getRelativeBalance() {
		return relativeBalance;
	}

	public void setRelativeBalance(BigDecimal relativeBalance) {
		this.relativeBalance = relativeBalance;
	}

	public int getNoOfTransactions() {
		return noOfTransactions;
	}

	public void setNoOfTransactions(int noOfTransactions) {
		this.noOfTransactions = noOfTransactions;
	}
}
