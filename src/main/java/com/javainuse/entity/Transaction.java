package com.javainuse.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "transaction")
public class Transaction {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private	Long transactionId;
	private String fromAccountId;
	private String toAccountId;
	private Calendar createAt;
	private BigDecimal amount;
	@Enumerated(STRING)
	private TransactionType transactionType;
	private Long relatedTransaction;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}

	public Calendar getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Calendar createAt) {
		this.createAt = createAt;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Long getRelatedTransaction() {
		return relatedTransaction;
	}

	public void setRelatedTransaction(Long relatedTransaction) {
		this.relatedTransaction = relatedTransaction;
	}
}
