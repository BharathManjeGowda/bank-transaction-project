package com.javainuse.entity;

public enum TransactionType {

    PAYMENT,
    REVERSAL;

}
