package com.javainuse.exception;

public class TransactionCustomException extends Exception {

    public TransactionCustomException(String message) {
        super(message);
    }

    public TransactionCustomException(String message, Throwable cause) {
        super(message, cause);
    }

}
