package com.javainuse.service;

import com.javainuse.entity.SearchCriteria;
import com.javainuse.entity.SearchResponse;
import com.javainuse.exception.TransactionCustomException;
import com.javainuse.entity.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TransactionService {

    List<Transaction> getTransactionsList();

    Long saveTransaction(Transaction transaction);

    Transaction findTransactionById(Long id);

    void deleteTransactionById(Long id) throws TransactionCustomException;

    Transaction updateTransaction(Long id, Transaction transaction) throws TransactionCustomException;

    boolean validateRequest(Transaction transaction);

    boolean validateSearchRequest(SearchCriteria criteria);

    SearchResponse getRelativeBalance(SearchCriteria criteria);

}
