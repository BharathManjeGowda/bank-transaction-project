package com.javainuse.service;

import com.javainuse.dao.TransactionRepository;
import com.javainuse.entity.SearchCriteria;
import com.javainuse.entity.SearchResponse;
import com.javainuse.exception.TransactionCustomException;
import com.javainuse.entity.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private TransactionRepository transactionData;

    @Override
    public SearchResponse getRelativeBalance(SearchCriteria criteria) {
        SearchResponse response = new SearchResponse();
        BigDecimal balance = BigDecimal.ZERO;
        Integer noOfTransactions = 0;
        List<Transaction> outgoingTransactions = transactionData.findAllOutgoingTransactions(criteria.getAccountId(), criteria.getFromDate(), criteria.getToDate());
        List<Transaction> incomingTransactions = transactionData.findAllIncomingTransactions(criteria.getAccountId(), criteria.getFromDate(), criteria.getToDate());
        List<Long> reverseTransactions = transactionData.findAllByTransactionTypeAndRelatedTransaction(criteria.getAccountId());

        for(Transaction transaction: outgoingTransactions) {
            if(!reverseTransactions.contains(transaction.getTransactionId())) {
                balance = balance.subtract(transaction.getAmount());
                noOfTransactions++;
            }
        }

        for(Transaction transaction: incomingTransactions) {
            if(!reverseTransactions.contains(transaction.getTransactionId())) {
                balance = balance.add(transaction.getAmount());
                noOfTransactions++;
            }
        }
        response.setNoOfTransactions(noOfTransactions);
        response.setRelativeBalance(balance);
        return response;
    }

    @Override
    public boolean validateSearchRequest(SearchCriteria criteria){
        if(criteria == null || criteria.getAccountId() == null || criteria.getFromDate() == null || criteria.getToDate() == null) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public Long saveTransaction(Transaction transaction) {
        Transaction newTransaction = new Transaction();
        try {
            newTransaction = transactionData.save(transaction);
        } catch (Exception ex){
            LOG.debug("Error while creating new transaction" + ex);
        }

        return newTransaction.getTransactionId();
    }

    @Override
    public List<Transaction> getTransactionsList() {
        List<Transaction> allTransactions = transactionData.findAll();
        LOG.debug("Total number fo transactions" + allTransactions.size());
        return allTransactions;
    }

    @Override
    public Transaction findTransactionById(Long id) {
        Transaction transaction = transactionData.findByTransactionId(id);
        return transaction;
    }

    @Override
    @Transactional
    public void deleteTransactionById(Long id) throws TransactionCustomException {
        try {
            transactionData.delete(id);
        }
        catch (EmptyResultDataAccessException ex){
            LOG.debug("No transaction was found for the id" + id);
            throw new TransactionCustomException("No transaction was found for the id" + id);
        }
        catch (Exception ex){
            LOG.debug("Error while deleting a transaction" + ex);
        }
    }

    @Override
    @Transactional
    public Transaction updateTransaction(Long id, Transaction transaction) throws TransactionCustomException {
        try {
            transaction.setTransactionId(id);
            transaction = transactionData.save(transaction);
        }
        catch (EmptyResultDataAccessException ex){
            LOG.debug("No transaction was found for the id" + id);
            throw new TransactionCustomException("No transaction was found for the id" + id);
        }
        catch (Exception ex){
            LOG.debug("Error while deleting a transaction" + ex);
        }
        return transaction;
    }

    @Override
    public boolean validateRequest(Transaction transaction){
        return true;
    }

}
