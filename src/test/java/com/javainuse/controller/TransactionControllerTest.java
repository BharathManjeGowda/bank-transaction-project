package com.javainuse.controller;

import com.javainuse.entity.SearchCriteria;
import com.javainuse.entity.SearchResponse;
import com.javainuse.service.TransactionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.Calendar;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TransactionControllerTest {

	@InjectMocks
	private TransactionController transactionController;

	@Mock
	private TransactionService transactionService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

    @Test
    public void testFetchTransaction() {
        Mockito.when(transactionService.validateSearchRequest(any(SearchCriteria.class))).thenReturn(true);
        Mockito.when(transactionService.getRelativeBalance(any(SearchCriteria.class))).thenReturn(new SearchResponse());
        transactionController.fetchTransaction(getSearchCriteria());
        verify(transactionService, times(1)).getRelativeBalance(any(SearchCriteria.class));
    }

    public SearchCriteria getSearchCriteria(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAccountId("12313");
        searchCriteria.setFromDate(Calendar.getInstance());
        searchCriteria.setToDate(Calendar.getInstance());
        return searchCriteria;
    }
}
