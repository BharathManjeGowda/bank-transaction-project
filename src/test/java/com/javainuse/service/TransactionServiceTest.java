package com.javainuse.service;

import com.javainuse.dao.TransactionRepository;
import com.javainuse.entity.SearchCriteria;
import com.javainuse.entity.SearchResponse;
import com.javainuse.entity.Transaction;
import com.javainuse.entity.TransactionType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TransactionServiceTest {

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Mock
    private TransactionRepository transactionData;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetRelativeBalance() {
        Mockito.when(transactionData.findAllOutgoingTransactions(anyString(), any(Calendar.class), any(Calendar.class))).thenReturn(listOfTransaction());
        Mockito.when(transactionData.findAllIncomingTransactions(anyString(), any(Calendar.class), any(Calendar.class))).thenReturn(new ArrayList<Transaction>());
        SearchResponse response = transactionService.getRelativeBalance(getSearchCriteria());
        verify(transactionData, times(1)).findAllByTransactionTypeAndRelatedTransaction(Matchers.any(String.class));
        Assert.assertEquals(response.getNoOfTransactions(), 1);
        Assert.assertEquals(response.getRelativeBalance(), new BigDecimal(-123));
    }

    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAccountId("12313");
        searchCriteria.setFromDate(Calendar.getInstance());
        searchCriteria.setToDate(Calendar.getInstance());
        return searchCriteria;
    }

    private List<Transaction> listOfTransaction() {
        List<Transaction> outgoingTransactions = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setTransactionId(12l);
        transaction.setAmount(new BigDecimal(123));
        transaction.setCreateAt(Calendar.getInstance());
        transaction.setTransactionType(TransactionType.PAYMENT);
        outgoingTransactions.add(transaction);
        return outgoingTransactions;
    }
}
